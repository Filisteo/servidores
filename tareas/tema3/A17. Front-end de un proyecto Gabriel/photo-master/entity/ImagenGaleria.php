<?php

class ImagenGaleria{
    
    private $nombre;
    private $descripcion;

    private $numVisualizaciones;


    private $numLikes;
  
  
    private $numDownloads;

    private const RUTA_IMAGENES_PORTFOLIO ="images/index/portfolio/";
    private const RUTA_IMAGENES_GALERIA ="images/index/gallery/";

    public function __construct($nombre, $descripcion, $numVisualizaciones=0, $numLikes=0, $numDownloads=0)
    {
        $this->nombre = $nombre;

        $this->descripcion = $descripcion;

        $this->numVisualizaciones = $numVisualizaciones;

        $this->numLikes = $numLikes;

        $this->numDownloads = $numDownloads;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of numVisualizaciones
     */ 
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    /**
     * Set the value of numVisualizaciones
     *
     * @return  self
     */ 
    public function setNumVisualizaciones($numVisualizaciones)
    {
        $this->numVisualizaciones = $numVisualizaciones;

        return $this;
    }

    /**
     * Get the value of numLikes
     */ 
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
     * Set the value of numLikes
     *
     * @return  self
     */ 
    public function setNumLikes($numLikes)
    {
        $this->numLikes = $numLikes;

        return $this;
    }

    /**
     * Get the value of numDownloads
     */ 
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }

    /**
     * Set the value of numDownloads
     *
     * @return  self
     */ 
    public function setNumDownloads($numDownloads)
    {
        $this->numDownloads = $numDownloads;

        return $this;
    }

    
    public function __toString(){
        return $this -> getNombre();
        return $this -> getDescripcion();
    }

    
    public function getURLPortfolio() : string
    {
        return self::RUTA_IMAGENES_PORTFOLIO.$this->getNombre();
    }

    public function getURLGaleria() : string
    {
        return self::RUTA_IMAGENES_GALERIA.$this->getNombre();
    }

}

//1. La diferencia es el grado de visibilidad y de protección que se le da a un elemento, restrigiendole su acceso dpendiendo del grado de protección que le demos
//2. Un metodo que permite realizar acciones dependiendo de los eventos que se activen.
//3. No
//4. Object of class ImagenGaleria could not be converted to string.
//self hace referencia a la clase para así mandar llamar funciones estáticas. this hace referencia a un objeto ya instanciado para mandar llamar funciones de cualquier otro tipo.

//$image = new ImagenGaleria('1.jpg', 'Grande');
//echo $image;
?>