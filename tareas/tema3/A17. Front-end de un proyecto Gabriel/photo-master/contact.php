<?php
include __DIR__ . "/utils/utils.php";
// VARIABLES GLOBALES

$name = isset($_POST['name']) ? htmlentities($_POST['name']) : '';

$lastName = isset($_POST['last-name']) ? htmlentities($_POST['last-name']) : '';

$email = isset($_POST['email']) ? htmlentities($_POST['email']) : '';

$subject = isset($_POST['subject']) ? htmlentities($_POST['subject']) : '';

$mg = isset($_POST['mg']) ? htmlentities($_POST['mg']) : '';

// ARRAY Y DEFNICIÓN DEL ARRAY

$errores = array();

$errores[0] = 'Campo Obligatorio';
$errores[1] = "Email incorrecto";

// FUNCIONES CHECK

function checkName($name, $errores)
{
    if (isset($_POST['name']) && empty($name)) {
        return $errores[0];
    } elseif (empty($errores)) {
        return "";
    }
}

function checkSubject($subject,$errores){

    if (isset($_POST['subject']) && empty($subject)) {
        return $errores[0];
    } elseif (empty($errores)) {
        return "";
    }
}

function checkEmail($email,$errores)
{
    if (isset($_POST['email']) && empty($email)) {
        return $errores[0];
    } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) == false && !empty($email)) {
        return $errores[1];
    }
}

function Pass($name,$lastName,$email,$subject,$mg){

    if(isset($_POST['name']) && !empty($name) && 
       isset($_POST['subject']) && !empty($subject) && 
       (isset($_POST['email']) && !empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL) != false)
       ){
           
        echo "Name: ".$name;
        echo "<br>";
        echo "Last Name: ".$lastName;
        echo "<br>";
        echo "Email: ".$email;
        echo "<br>";
        echo "Subject: ".$subject;
        echo "<br>";
        echo "Message: ".$mg;
    }
}

require "views/contact.view.php";
?>