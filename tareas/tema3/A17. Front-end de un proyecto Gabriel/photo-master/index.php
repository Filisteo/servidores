<?php
include __DIR__ . "/utils/utils.php";
require "entity/ImagenGaleria.php";
require_once "entity/Asociado.php";

$asociados = array();

$images = array();

for ($i = 1; $i <= 12; $i++) {
    $images[$i] = new ImagenGaleria($i . ".jpg", "descripción " . $i, rand(0, 10000), rand(0, 100000), rand(0, 100000));
}

$nombres = array(
    1 => "Jose",
    2 => "Marcos",
    3 => "Manuel",
    4 => "Paco",
    5 => "Daniel",
    6 => "Alejandro",
);

$img = 1;

for ($a = 1; $a <= 6; $a++) {

    if ($img > 3) {
        $img = 1;
    }

    $asociados[$a] = new Asociado($nombres[$a], "log" . $img . ".jpg", "Hola Mundo");

    $img++;
}


$partners = asociados($asociados);

require "views/index.view.php";
