<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>autos.php</title>
</head>
<style>
    table, th, td {
    border:1px solid black;
    margin: auto;
    }
</style>
<body>
    <h1 style="text-align: center;">Autos</h1>

    <form method="post" style="text-align: center;">
        <p>
            <input type="submit" style="margin-left: 5px;" name="Logout" value="Logout">
            <input type="submit" style="margin-left: 5px;" name="Insert" value="Insert">
            <br>
    </form>
            <table>
            <tr>
                <th>ID</th>
                <th>Make</th>
                <th>Year</th>
                <th>Mileage</th>
                <th>Delete</th>
                <th>Actualizar</th>
            </tr>
            <?php 
                $rows = $auto->getAutos();
                foreach( $rows as $row ){ ?>
                <tr>
                    <td><?=$row->auto_id;?></td>
                    <td><?=$row->make;?></td>
                    <td><?=$row->year;?></td>
                    <td><?=$row->mileage;?></td>
                    <td>
                    <form method='post' style="text-align: center;">
                    <input type='hidden' name='id_eliminar' value="<?= $row->auto_id?>">
                    <input name="deleteee" type='submit' id="delete" value='Eliminar'></td>
                    </form>
                    <td>
                    <form method='post' style="text-align: center;">
                    <input type='hidden' name='id_updatear' value="<?= $row->auto_id?>">
                    <input name="updatee" type='submit' id="update" value='Actualizar'></td>
                    </form>
                </tr>
                <div style="text-align: center;">
                <?php 
                }
                
                if (isset($_SESSION['borrado']) ) {
                    echo('<div><p style="color: green;">'.htmlentities($_SESSION['borrado'])."</p></div>\n");
                    unset($_SESSION['borrado']);
                }

                if (isset($_SESSION['update']) ) {
                    echo('<div><p style="color: green;">'.htmlentities($_SESSION['update'])."</p></div>\n");
                    unset($_SESSION['update']);
                }

                if (isset($_SESSION['success']) ) {
                    echo('<div><p style="color: green;">'.htmlentities($_SESSION['success'])."</p></div>\n");
                    unset($_SESSION['success']);
                }
                 ?>
                </div>
                <div style="text-align: center;"><?="Número de columnas".$auto->rowCount();?></div>
            </table>
        </p>
</body>
</html>