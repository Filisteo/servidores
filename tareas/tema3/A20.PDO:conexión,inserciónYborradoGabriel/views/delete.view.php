<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Delete</title>
</head>
<style>
    table, th, td {
    border:1px solid black;
    margin: auto;
    }
</style>
<body>
    <div style="text-align: center;">
        <table>
            <tr>
                <th>ID</th>
                <th>Make</th>
                <th>Year</th>
                <th>Mileage</th>
            </tr>
            <tr>
                <td><?=$id?></td>
                <td><?=$auto->getAuto($id)->getMake();?></td>
                <td><?=$auto->getAuto($id)->getYear();?></td>
                <td><?=$auto->getAuto($id)->getMileage();?></td>
            </tr>
        </table>
        <br>
        <form method="POST">
            <input type="submit" value="Delete" name="Delete">
            <input type="submit" value="Cancel" name="Cancel">
        </form>
    </div>
    <?php
    if(isset($_POST['Delete'])){
        delete($auto);
    }

    if(isset($_POST['Cancel'])){
        header("Location:autos.php");
    }
    ?>
</body>
</html>