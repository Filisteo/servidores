<?php
session_start();
include __DIR__."/models/Auto.php";

$auto = new auto();
$auto->makeConnection();

$id = isset($_SESSION['id']) ? htmlspecialchars(trim($_SESSION['id'])) : '';

if (! isset($_SESSION['email']) ) {
    die('ACCESS DENIED');
}

$delete = isset($_POST['Delete']) ? htmlspecialchars(trim($_POST['Delete'])) : '';
$cancel = isset($_POST['Cancel']) ? htmlspecialchars(trim($_POST['Cancel'])) : '';

//DELETE DATA
function delete($auto){

    $id_eliminar = isset($_SESSION['id']) ? htmlspecialchars(trim($_SESSION['id'])) : '';
    
    $auto->delAuto($id_eliminar);

    $_SESSION['borrado'] = "Registro borrado";
    header("Location: autos.php");
    die();

}

require_once "views/delete.view.php";
?>