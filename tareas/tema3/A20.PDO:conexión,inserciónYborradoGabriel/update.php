<?php
session_start();
include __DIR__."/models/Auto.php";

$auto = new auto();
$auto->makeConnection();

$id = isset($_GET["id"]) ? htmlspecialchars(trim($_GET['id'])) : '';

if (! isset($_SESSION['email']) ) {
    die('ACCESS DENIED');
}

function updateAuto($auto, $id){

    $year = isset($_POST['Year']) ? htmlspecialchars(trim($_POST['Year'])) : '';
    $mileage = isset($_POST['Mileage']) ? htmlspecialchars(trim($_POST['Mileage'])) : '';
    $make = isset($_POST['Make']) ? htmlspecialchars(trim($_POST['Make'])) : '';

    $year = intval($year);
    $mileage = intval($mileage); 

    $updateAuto = new auto($make,$year,$mileage);

    $auto->updateAuto($updateAuto,$id);
    
    $_SESSION['update'] = "Registro updateado";
    header("Location: autos.php");
    die();
}

if(isset($_POST['update']) && !empty($_POST['Make']) && !empty($_POST['Year']) && !empty($_POST['Mileage'])){
    updateAuto($auto, $id);
}

if(isset($_POST['Cancel'])){
    header("Location:autos.php");
}

require_once "views/update.view.php";
?>