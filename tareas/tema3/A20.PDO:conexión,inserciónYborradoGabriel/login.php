<?php
session_start();
//VARIABLES GLOBALES
$salt = 'XyZzy12*_';

$stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';

$name = isset($_POST['name']) ? htmlspecialchars(trim($_POST['name'])) : '';


$password = isset($_POST['password']) ? htmlspecialchars(trim($_POST['password'])): '';


$email = isset($_POST['email']) ? htmlspecialchars(trim($_POST['email'])) : '';

//SESIONES

$_SESSION['name'] = $name;
$_SESSION['password'] = $password;

//COMPROBANTES

if(isset($_POST['name']) && empty($name)){
    $_SESSION['error'] = " Se requiere nombre de usuario";
    header("Location: login.php");
    return;
}

if((isset($_POST['password']) && empty($password)) || (isset($_POST['email']) && empty($email))){
    $_SESSION['error'] = " Se requiere contraseña y email";
    header("Location: login.php");
    return;
}

if((isset($_POST['password']) && !empty($password)) && hash('md5', $salt.$_POST['password']) != $stored_hash){
    $_SESSION['error'] = ' Contraseña incorrecta';
    error_log("Login Fail ".$email." ".$password,0);
    header("Location: login.php");
    return;

}if((isset($_POST['email']) && !empty($email)) && !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
    $_SESSION['error'] = " Correo invalido";
    header("Location: login.php");
    return;
}

if(hash('md5', $salt.$password) == $stored_hash && filter_var($email,FILTER_VALIDATE_EMAIL)){
    error_log("Login Succes ".$email,0);
    $_SESSION['email'] = $email;
    header("Location: autos.php");
    return;
}

//MENSAJE FLASH

if ( isset($_SESSION['error']) ) {
    echo('<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n");
    unset($_SESSION['error']);
  }


// $md5 = hash('md5', 'XyZzy12*_php123');

echo " Contraseña = php123";

require_once "views/login.view.php";
session_destroy();
?>
