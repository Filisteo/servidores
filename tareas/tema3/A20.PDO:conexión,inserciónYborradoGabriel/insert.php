<?php
session_start();
include __DIR__."/models/Auto.php";

$auto = new auto();
$auto->makeConnection();


if (! isset($_SESSION['email']) ) {
    die('ACCESS DENIED');
  }

if (isset($_POST['cancel'])) {
    header("Location: autos.php");
    die();
}

//INSERT DATA

function insert_Data($auto){
    $year = isset($_POST['Year']) ? htmlspecialchars(trim($_POST['Year'])) : '';
    $mileage = isset($_POST['Mileage']) ? htmlspecialchars(trim($_POST['Mileage'])) : '';
    $make = isset($_POST['Make']) ? htmlspecialchars(trim($_POST['Make'])) : '';

    $year = intval($year);
    $mileage = intval($mileage); 

    $newAuto = new auto($make,$year,$mileage);
    $auto->addAuto($newAuto);
        

    $_SESSION['success'] = "Record inserted";
    header("Location: autos.php");
    die();
}

require_once "views/insert.view.php";
?>