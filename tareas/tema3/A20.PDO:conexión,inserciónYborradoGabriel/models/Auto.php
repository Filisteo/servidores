<?php

include_once __DIR__."/../lib/Database.php";

 class auto {
     
    private $db;

    private $make;

    private $year;

    private $mileage;


    public function __construct($make = '', $year = 0, $mileage = 0)

    {

        $this->make = $make;

        $this->year = $year;

        $this->mileage = $mileage;

    }


    
    public function makeConnection()

    {

        $this->db = new Database();

    }

    
    public function getAutos()

    {    
        $this->db->query("SELECT auto_id, make, year,mileage FROM autos");

        $results = $this->db->resultSet('Auto');

        return $results;
    }

    public function getAuto($id)

    {
        $this->db->query("SELECT auto_id, make, year,mileage FROM autos WHERE auto_id = $id");
        $results = $this->db->single('auto');
        return $results;
    }

    
    public function addAuto($auto)

    {

        $this->db->query('insert into autos (make,year,mileage) Values (:mk,:yr,:mi)');

        $this->db->bind(':mk', $auto->getMake());

        $this->db->bind(':yr', $auto->getYear());

        $this->db->bind(':mi', $auto->getMileage());

        $this->db->execute();

    }

    public function delAuto($id)

    {
        $this->db->query("DELETE FROM autos WHERE auto_id = :id");
        $this->db->bind(':id', $id);
        $this->db->execute();
    }

    public function rowCount(){
        $this->db->query("SELECT * FROM autos");
        return count($this->db->resultSet());
    }
    
    
    public function updateAuto($auto, $id)
    { 
        $this->db->query("UPDATE autos SET auto_id = $id, make = :mk, year = :yr , mileage = :mi WHERE auto_id = :id");
        
        $this->db->bind(':id', $id);

        $this->db->bind(':mk', $auto->getMake());

        $this->db->bind(':yr', $auto->getYear());

        $this->db->bind(':mi', $auto->getMileage());

        $this->db->execute();
    }


    /**
     * Get the value of make
     */ 
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Get the value of year
     */ 
    public function getYear()
    {
        return $this->year;
    }

        /**
         * Get the value of mileage
         */ 
    public function getMileage()
    {
            return $this->mileage;
    }

    /**
     * Set the value of make
     *
     * @return  self
     */ 
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

        /**
         * Set the value of year
         *
         * @return  self
         */ 
        public function setYear($year)
        {
                $this->year = $year;

                return $this;
        }

        /**
         * Set the value of mileage
         *
         * @return  self
         */ 
        public function setMileage($mileage)
        {
                $this->mileage = $mileage;

                return $this;
        }
 }

 

?>