<?php
session_start();
include __DIR__."/models/Auto.php";

$auto = new auto();
$auto->makeConnection();


if (! isset($_SESSION['email']) ) {
    die('ACCESS DENIED');
  }

if (isset($_POST['Logout'])) {
    header("Location: logout.php");
    die();
}

if (isset($_POST['Insert'])) {
    header("Location: insert.php");
    die();
}



if(isset($_POST['id_eliminar'])){
    $_SESSION['id'] = $_POST['id_eliminar'];
    header('Location: delete.php');
    die();
}

if(isset($_POST['id_updatear'])){
    header('location: update.php?id=' . urlencode($_POST['id_updatear'])) and die();
}

require_once "views/autos.view.php";
?>