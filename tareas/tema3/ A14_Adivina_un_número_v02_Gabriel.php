<?php

$oldguess = isset($_POST['guess']) ? $_POST['guess'] : '';

$oldguess = '';
$message = false;
if (isset($_POST['guess'])) {
    // Nifty trick
    $oldguess = $_POST['guess'] + 0;
    if ($oldguess == 42) {
        $message = "Great job!";
    } else if ($oldguess < 42) {
        $message = "Too low";
    } else {
        $message = "Too high...";
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Guess my number Gabriel</title>
</head>

<body>
    <p>Guessing game...</p>
    <?php
        if ($message !== false) {
            echo ("<p>$message</p>\n");
        }
    ?>
    <form method="post">
        <p>
            <label for="guess">Input Guess</label>
            <input type="text" name="guess" id="guess" value="<?= htmlentities($oldguess) ?>" />
        </p>

        <input type="submit" />

    </form>
</body>

</html>

