<?php

$salt = 'XyZzy12*_';

$stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';

$name = isset($_POST['name']) ? $_POST['name'] : '';

$password = isset($_POST['password'])? '' : '';

if(empty($_POST['name'])||empty($_POST['password'])){
    echo "Se requiere nombre de usuario y clave para acceder.";
} else if(hash('md5',$salt.$_POST["password"]) !=$stored_hash){
    echo "Contraseña incorrecta";
} else{
    header("Location: game.php?name=".urlencode($_POST['name']));
}

// $md5 = hash('md5', 'XyZzy12*_php123');

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <h1>Login</h1>
    <form method="POST">
        <p><label for="na">Name:</label>
        <input type="text" name="name" id="na" value="<?= htmlentities(trim($name))?>" style="width: 200px;"/></p>
        <br>
        <p><label for="pw">Password:</label>
        <input type="password" id="pw" name="password" value="<?php htmlentities(trim($password))?>"/></p>
        <input type="submit">
    </form>

</body>
</html>