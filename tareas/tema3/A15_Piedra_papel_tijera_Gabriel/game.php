<?php

$name = htmlentities($_GET['name']);

if (empty(htmlentities($_GET['name']))) {
    die("Falta el nombre del parámetro");
}

if (isset($_POST['Logout'])) {
    header("Location: index.php");
}

//----------------------------------------


function check($computer, $human)
{
 
    if ($human == "Test") {
        test();
    }else{
        echo "Humano=$human Ordenador=$computer Result=";
        return game($computer, $human);
    }
}

function game($computer, $human){

    if($human == $computer) {
        return 'Tie';
    } elseif (($human == 'Rock' && $computer == 'Paper') || ($human == 'Paper' && $computer == 'Scissors') || ($human == 'Scissors' && $computer == 'Rock')) {
        return 'Defeat';
    } else {
        return 'Victory';
    }  

}

function imprimir($human, $computer, $r){

    return print "Humano=$human Ordenador=$computer Result=$r\n";

}

function test()
{

    $names = [
        "Rock",
        "Paper",
        "Scissors"
    ];

    for ($computer = 0; $computer < 3; $computer++) {

        for ($human = 0; $human < 3; $human++) {

            $r = game($names[$computer], $names[$human]);

            imprimir($names[$human], $names[$computer], $r);
        }
    }
}

function rando(){

    $n = [
        "Rock",
        "Paper",
        "Scissors"
    ];
    
    $random = rand(0, 2);

    $computer = $n[$random];

    return $computer;
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Game</title>
</head>

<body>
    <h1 style="text-align: center;">Rock, Paper or Scissors</h1>
    <p style="text-align: center;">Welcome: <?= $name ?></p>
    <form method="post" style="text-align: center;">
        <p><label for="inp06">
                <select name="options" id="inp06">
                    <option value="index">Select</option>
                    <option value="Rock">Rock</option>
                    <option value="Paper">Paper</option>
                    <option value="Scissors">Scissors</option>
                    <option value="Test">Test</option>
                </select>
            </label>
            <input type="submit" style="text-align: center;" name="Play" value="Play">
            <input type="submit" style="margin-left: 5px;" name="Logout" value="Logout">
            <br>
            <textarea name="view" id="" cols="30" rows="2">
            <?php
            echo check(rando(), $_POST['options']);
            ?>
            </textarea>
        </p>
    </form>
</body>

</html>